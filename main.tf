provider "google" {                                                                 
credentials = "${file("account.json")}"                                             
project = "extended-legend-239009"                                                  
region = "us-west1"                                                                 
}                                                                                   
                                                                                    
                                                                                    
provider "google-beta" {                                                            
                                                                                    
credentials = "${file("account.json")}"                                             
                                                                                    
project = "extended-legend-239009"                                                  
                                                                                    
region = "us-west1"                                                                 
                                                                                    
}                                                                                   
                                                                                    
variable "image_url" {                                                              
  default = "centos-7-v20190423"                                                    
}                                                                                   
                                                                                    
resource "google_compute_disk" "osd1" {                                             
  name = "osd1"                                                                     
  type = "pd-standard"                                                              
  zone = "us-west1-a"                                                               
  size = "10"                                                                       
}                                                                                   
                                                                                    
resource "google_compute_instance" "ceph-admin" {                                   
  name         = "ceph-admin"                                                       
  machine_type = "g1-small"                                                         
  zone         = "us-west1-a"                                                       
                                                                                    
  boot_disk {                                                                       
    initialize_params {                                                             
     image = "${var.image_url}"                                                     
        }                                                                           
  }                                                                                 
                                                                                    
  attached_disk {                                                                   
    source = "${google_compute_disk.osd1.name}"                                     
  }                                                                                 
                                                                                    
  network_interface {                                                               
    network       = "default"                                                       
    access_config = {}                                                              
  }                                                                                 
}                                                                                   
                                                                                    
resource "google_compute_disk" "osd2" {                                             
  name = "osd2"                                                                     
  type = "pd-standard"                                                              
  zone = "us-west1-a"                                                               
  size = "10"                                                                       
}                                                                                   
                                                                                    
resource "google_compute_instance" "ceph-node1" {                                   
  name         = "ceph-node1"                                                       
  machine_type = "g1-small"                                                         
  zone         = "us-west1-a"                                                       
                                                                                    
  boot_disk {                                                                       
    initialize_params {                                                             
     image = "${var.image_url}"                                                     
        }                                                                           
  }                                                                                 
                                                                                    
  attached_disk {                                                                   
    source = "${google_compute_disk.osd2.name}"                                     
  }                                                                                 
                                                                                    
  network_interface {                                                               
    network       = "default"                                                       
    access_config = {}                                                              
  }                                                                                 
}                                                                                   
                                                                                    
resource "google_compute_disk" "osd3" {                                             
  name = "osd3"                                                                     
  type = "pd-standard"                                                              
  zone = "us-west1-a"                                                               
  size = "10"                                                                       
}                                                                                   
                                                                                    
resource "google_compute_instance" "ceph-node2" {                                   
  name         = "ceph-node2"                                                       
  machine_type = "g1-small"                                                         
  zone         = "us-west1-a"                                                       
                                                                                    
  boot_disk {                                                                       
    initialize_params {                                                             
     image = "${var.image_url}"                                                     
        }                                                                           
  }                                                                                 
                                                                                    
  attached_disk {                                                                   
    source = "${google_compute_disk.osd3.name}"                                     
  }                                                                                 
                                                                                    
  network_interface {                                                               
    network       = "default"                                                       
    access_config = {}                                                              
  }                                                                                 
}                                                                                   
                                                                                    
resource "google_compute_disk" "osd4" {                                             
  name = "osd4"                                                                     
  type = "pd-standard"                                                              
  zone = "us-west1-a"                                                               
  size = "10"                                                                       
}                                                                                   
                                                                                    
resource "google_compute_instance" "ceph-node3" {                                   
  name         = "ceph-node3"                                                       
  machine_type = "g1-small"                                                         
  zone         = "us-west1-a"                                                       
                                                                                    
  boot_disk {                                                                       
    initialize_params {                                                             
     image = "${var.image_url}"                                                     
        }                                                                           
  }                                                                                 
                                                                                    
  attached_disk {                                                                   
    source = "${google_compute_disk.osd4.name}"                                     
  }                                                                                 
                                                                                    
  network_interface {                                                               
    network       = "default"                                                       
    access_config = {}                                                              
  }                                                                                 
}                                                                                   
                                                                                    
resource "google_compute_instance" "ceph-client" {                                  
  name         = "ceph-client"                                                      
  machine_type = "g1-small"                                                         
  zone         = "us-west1-a"                                                       
                                                                                    
  boot_disk {                                                                       
    initialize_params {                                                             
     image = "${var.image_url}"                                                     
        }                                                                           
  }                                                                                 
                                                                                    
                                                                                    
  network_interface {                                                               
    network       = "default"                                                       
    access_config = {}                                                              
  }                                                                                 
}                                                                                   